const XLSX = require('xlsx');
const fs = require('fs');

// path file
const sourceFilePath = './source/8212-WFT-170820230900.xlsx'; //ganti berdasarkan nama file
const resultFolderPath = './result';

// Membaca file Excel
const workbook = XLSX.readFile(sourceFilePath);

// Mendapatkan nama sheet dari file Excel
const sheetName = workbook.SheetNames[0];

// Mendapatkan data dari sheet
const worksheet = workbook.Sheets[sheetName];
const data = XLSX.utils.sheet_to_json(worksheet);

// Menghitung jumlah data per file
const maxRows = 10000;
const fileCount = Math.ceil(data.length / maxRows);

// Menulis file Excel baru untuk setiap 10 ribu row
for (let i = 0; i < fileCount; i++) {
  const startRow = i * maxRows;
  const endRow = (i + 1) * maxRows;
  const newData = data.slice(startRow, endRow);

  // Membuat file Excel baru
  const newWorkbook = XLSX.utils.book_new();
  const newWorksheet = XLSX.utils.json_to_sheet(newData);

  // Menambahkan nama sheet
  XLSX.utils.book_append_sheet(newWorkbook, newWorksheet, sheetName);

  // Menuliskan data ke dalam file Excel baru
  const fileName = `${resultFolderPath}/8212-WFT-170820230900-[${i+2}].xlsx`; //ganti berdasarkan nama file
  XLSX.writeFile(newWorkbook, fileName);

  console.log(`File ${fileName} berhasil ditulis.`);
}
