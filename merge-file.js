const XLSX = require('xlsx');

// Array of source Excel file paths
const sourceFilePaths = [
  './source/8212-WFT-170820230900-[9].xlsx',
  './source/8212-WFT-170820230900-[10].xlsx',
  './source/8212-WFT-170820230900-[11].xlsx',
  './source/8212-WFT-170820230900-[12].xlsx',
  './source/8212-WFT-170820230900-[13].xlsx',
  // Add more file paths as needed
];

// Array to store all data
let mergedData = [];

// Iterate through each source Excel file path and merge their data
sourceFilePaths.forEach(filePath => {
  const workbook = XLSX.readFile(filePath);
  const sheetName = workbook.SheetNames[0];
  const worksheet = workbook.Sheets[sheetName];
  const data = XLSX.utils.sheet_to_json(worksheet);
  mergedData = mergedData.concat(data);
});

// Create a new workbook
const mergedWorkbook = XLSX.utils.book_new();
const mergedWorksheet = XLSX.utils.json_to_sheet(mergedData);

// Add the merged data to the workbook
XLSX.utils.book_append_sheet(mergedWorkbook, mergedWorksheet, 'Sheet 1');

// Write the merged workbook to a new Excel file
const mergedFilePath = './result/8212-WFT-170820230900-[50000]-[2].xlsx'; // Path to save the merged file
XLSX.writeFile(mergedWorkbook, mergedFilePath);

console.log(`Merged file ${mergedFilePath} successfully created.`);